--Create 1 Stored Procedure to retrieve KPIs for specific campaign

USE [VELTI_DE];
GO
/****** Object:  StoredProcedure [workflow].[RetrieveCampaignKPIs]    Script Date: 31/05/2021 1:05:50 pm ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET XACT_ABORT ON;
GO
SET NOCOUNT ON;
GO

-- =============================================
-- Author:		<Panagiotis Markovits>
-- Create date: <21/05/2021>
-- Description:	<retrieve KPIs for specific campaign>
-- =============================================
CREATE PROCEDURE [workflow].[RetrieveCampaignKPIs]
	-- Stored procedure Parameters
	@CMP_ID int

AS

DECLARE @@ERRORMSG NVARCHAR(2048);

BEGIN
	BEGIN TRAN RetrieveCampaignKPIs
	  BEGIN TRY
      SET NOCOUNT ON;
        DECLARE @Sql NVARCHAR(MAX);
        DECLARE @Tablename NVARCHAR(64);
        SET @Tablename = N'CMP_BR_OUT_QUEUE_BD' + CAST(@CMP_ID AS varchar);
        SET @Sql = N'SELECT [ID], [STATUS], [DATE_SENT], [ERROR_STATUS] FROM ' + QUOTENAME(@Tablename);
        EXECUTE sp_executesql @Sql;
		IF XACT_STATE() = 1
			COMMIT TRAN RetrieveCampaignKPIs;
	  END TRY
	  BEGIN CATCH
  		SELECT @@ERRORMSG = ERROR_MESSAGE();
		IF XACT_STATE() = -1
			PRINT 'Error While Retrieving Campaign KPIs';
			ROLLBACK
		RAISERROR('Error While Retrieving Campaign KPIs: %s', 11, 1, @@ERRORMSG);
	  END CATCH
END