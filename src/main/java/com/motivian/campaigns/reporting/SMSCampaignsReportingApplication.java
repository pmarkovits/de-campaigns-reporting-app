package com.motivian.campaigns.reporting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SMSCampaignsReportingApplication {

    public static void main(String[] args) { SpringApplication.run(SMSCampaignsReportingApplication.class, args); }

}
