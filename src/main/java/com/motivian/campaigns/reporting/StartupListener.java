package com.motivian.campaigns.reporting;

import com.motivian.campaigns.reporting.services.interfaces.AutomatedReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StartupListener implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private AutomatedReportService reportService;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        this.reportService.sendWeeklyReport();
    }

}
