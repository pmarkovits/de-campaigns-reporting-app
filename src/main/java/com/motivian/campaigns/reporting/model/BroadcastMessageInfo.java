package com.motivian.campaigns.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Immutable
public class BroadcastMessageInfo {

    @Column(name = "ID")
    private @Id Integer id;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "DATE_SENT")
    private Date dateSent;

    @Column(name = "ERROR_STATUS")
    private String errorStatus;

}
