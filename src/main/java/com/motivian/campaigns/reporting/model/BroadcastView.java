package com.motivian.campaigns.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Immutable
@Table(name = "WEEKLY_CAMPAIGNS")
public class BroadcastView {

    @Column(name = "ID")
    private @Id Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CMP_ORGANIZATION_ID")
    private Integer organizationId;

    @Column(name = "CMP_MSG_TYPE_ID")
    private Integer msgTypeId;

    @Column(name = "NUM_OF_RECIPIENTS")
    private Integer numOfRecipients;

    @Column(name = "IS_FINALIZED")
    private Integer isFinalized;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "ERROR_STATUS")
    private String errorStatus;

}
