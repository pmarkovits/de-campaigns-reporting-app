package com.motivian.campaigns.reporting.repository;

import com.motivian.campaigns.reporting.model.BroadcastMessageInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface BroadcastMessageInfoRepository extends CrudRepository<BroadcastMessageInfo, Integer> {

    @Query(value = "EXEC RetrieveCampaignKPIs @CMP_ID = :cmpId", nativeQuery = true)
    ArrayList<BroadcastMessageInfo> retrieveCampaignKPIs(@Param("cmpId") Integer campaignId);

}
