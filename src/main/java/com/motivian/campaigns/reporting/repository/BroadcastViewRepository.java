package com.motivian.campaigns.reporting.repository;

import com.motivian.campaigns.reporting.model.BroadcastView;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.Date;

public interface BroadcastViewRepository extends CrudRepository<BroadcastView, Integer> {
    int countByOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore(Integer organizationId, Integer msgTypeId, Integer status, Integer isFinalized, Date timeframeStart, Date timeframeEnd);
    int countByNameContainingAndOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore(String name, Integer organizationId, Integer msgTypeId, Integer status, Integer isFinalized, Date timeframeStart, Date timeframeEnd);
    ArrayList<BroadcastView> findByOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore(Integer organizationId, Integer msgTypeId, Integer status, Integer isFinalized, Date timeframeStart, Date timeframeEnd);
}
