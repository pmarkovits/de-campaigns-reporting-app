package com.motivian.campaigns.reporting.services.impl;

import com.motivian.campaigns.reporting.model.BroadcastMessageInfo;
import com.motivian.campaigns.reporting.model.BroadcastView;
import com.motivian.campaigns.reporting.repository.BroadcastMessageInfoRepository;
import com.motivian.campaigns.reporting.repository.BroadcastViewRepository;
import com.motivian.campaigns.reporting.services.interfaces.AutomatedReportService;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.apache.commons.math3.util.Precision.round;
import static java.util.stream.Collectors.groupingBy;


@Component
public class AutomatedReportServiceImpl implements AutomatedReportService {

    @Autowired
    private BroadcastViewRepository broadcastViewRepository;

    @Autowired
    private BroadcastMessageInfoRepository broadcastMessageInfoRepository;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Environment environment;

    @Override
    public void sendWeeklyReport() {
        // select only finalized campaigns
        Integer isFinalized = 1;
        // that have been completed
        Integer status = 5;
        // since last Monday at midnight & until this Friday afternoon
        DateTime now = DateTime.now();
        Date mondayMidnight = now.withDayOfWeek(DateTimeConstants.MONDAY).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).toDate();
        Date fridayAfternoon = now.withDayOfWeek(DateTimeConstants.FRIDAY).withHourOfDay(17).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).toDate();
        // count campaigns per operator and message type
        ArrayList<BroadcastView> vfMMSCampaigns = this.broadcastViewRepository.findByOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore(1, 2, status, isFinalized, mondayMidnight, fridayAfternoon);
        ArrayList<BroadcastView> dtMMSCampaigns = this.broadcastViewRepository.findByOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore(2, 2, status, isFinalized, mondayMidnight, fridayAfternoon);
        int vfSMSCampaignsNum = this.broadcastViewRepository.countByOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore(1, 1, status, isFinalized, mondayMidnight, fridayAfternoon);
        int dtSMSCampaignsNum = this.broadcastViewRepository.countByOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore(2, 1, status, isFinalized, mondayMidnight, fridayAfternoon);
        int vfMMSCampaignsNum = vfMMSCampaigns.size();
        int dtMMSCampaignsNum = dtMMSCampaigns.size();
        // retrieve all MMS messages per operator
        ArrayList<BroadcastMessageInfo> vfMMSMessages = new ArrayList<>();
        ArrayList<BroadcastMessageInfo> dtMMSMessages = new ArrayList<>();
        for (BroadcastView view : vfMMSCampaigns) {
            vfMMSMessages.addAll(this.broadcastMessageInfoRepository.retrieveCampaignKPIs(view.getId()));
        }
        for (BroadcastView view : dtMMSCampaigns) {
            dtMMSMessages.addAll(this.broadcastMessageInfoRepository.retrieveCampaignKPIs(view.getId()));
        }
        // group and count messages by status and error status
        Map<Integer, Map<String, List<BroadcastMessageInfo>>> vfGroups = vfMMSMessages.stream().collect(groupingBy(BroadcastMessageInfo::getStatus, groupingBy(b -> b.getErrorStatus() == null ? "null" : b.getErrorStatus())));
        Map<Integer, Map<String, List<BroadcastMessageInfo>>> dtGroups = dtMMSMessages.stream().collect(groupingBy(BroadcastMessageInfo::getStatus, groupingBy(b -> b.getErrorStatus() == null ? "null" : b.getErrorStatus())));
        int vfTotal = vfMMSMessages.size();
        int dtTotal = dtMMSMessages.size();
        int vfSent = vfGroups.isEmpty() ? 0 : (vfGroups.get(5).get("null") != null ? vfGroups.get(5).get("null").size() : 0);
        int vfFailed = vfGroups.isEmpty() ? 0 : (vfGroups.get(5).get("-1") != null ? vfGroups.get(5).get("-1").size() : 0);
        int dtSent = dtGroups.isEmpty() ? 0 : (dtGroups.get(5).get("null") != null ? dtGroups.get(5).get("null").size() : 0);
        int dtFailed = dtGroups.isEmpty() ? 0 : (dtGroups.get(5).get("-1") != null ? dtGroups.get(5).get("-1").size() : 0);
        int totalSent = vfSent + dtSent;
        int totalFailed = vfFailed + dtFailed;
        int totalMessages = vfTotal + dtTotal;
        double sentPercentage = 0;
        double failedPercentage = 0;
        if (totalSent > 0 && totalMessages > 0) {
            sentPercentage = round(((double) totalSent/(double) totalMessages) * 100, 2);
        }
        if (totalFailed > 0 && totalMessages > 0) {
            failedPercentage = round(((double) totalFailed/(double) totalMessages) * 100, 2);
        }
        // count cloned campaigns per operator and message type
        int vfSMSClonedCampaignsNum = this.broadcastViewRepository.countByNameContainingAndOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore("CLONE", 1, 1, status, isFinalized, mondayMidnight, fridayAfternoon);
        int dtSMSClonedCampaignsNum = this.broadcastViewRepository.countByNameContainingAndOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore("CLONE", 2, 1, status, isFinalized, mondayMidnight, fridayAfternoon);
        int vfMMSClonedCampaignsNum = this.broadcastViewRepository.countByNameContainingAndOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore("CLONE", 1, 2, status, isFinalized, mondayMidnight, fridayAfternoon);
        int dtMMSClonedCampaignsNum = this.broadcastViewRepository.countByNameContainingAndOrganizationIdAndMsgTypeIdAndStatusAndIsFinalizedAndStartDateAfterAndStartDateBefore("CLONE", 2, 2, status, isFinalized, mondayMidnight, fridayAfternoon);

        String subject = "[VIA.ENGAGE] Report: Weekly KPIs";

        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");

            String[] recipientList = new String[4];
            recipientList[0] = "support_campaigns@motivian.com";
            recipientList[1] = "mehmet.kozan@drwolfcommunications.com";
            recipientList[2] = "samuel.keim@drwolfcommunications.com";
            recipientList[3] = "stephan.werner@drwolfcommunications.com";

            String template = "<ul><li>Total MMS recipients per operator, successfully sent MMS, failed MMS</li></ul>"
                            + "<table cellspacing='15' cellpadding='10' style='text-align: center;border: thin solid #2BB673;border-style: solid;border-collapse: collapse;'>"
                            + "<tr>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">OPERATOR</td>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\"># MMS RECIPIENTS</td>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\"># SENT MMS</td>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\"># FAILED MMS</td>"
                            + "</tr><tr>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">VODAFONE DE</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr><tr>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">DEUTSCHE TELEKOM</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr><tr>"
                            + "<td style=\"font-weight:bold;width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">TOTAL</td>"
                            + "<td style=\"font-weight:bold;width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "<td style=\"font-weight:bold;width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s (%s%%)</td>"
                            + "<td style=\"font-weight:bold;width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s (%s%%)</td>"
                            + "</tr></table><br>"
                            + "<ul><li>Total number of (finalized) campaigns per message type per operator</li></ul>"
                            + "<table cellspacing='15' cellpadding='10' style='text-align: center;border: thin solid #2BB673;border-style: solid;border-collapse: collapse;'>"
                            + "<tr>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">OPERATOR</td>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">MESSAGE TYPE</td>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\"># CAMPAIGNS</td>"
                            + "</tr><tr>"
                            + "<td rowspan='2' style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">VODAFONE DE</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">SMS</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr><tr>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">MMS</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr><tr>"
                            + "<td rowspan='2' style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">DEUTSCHE TELEKOM</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">SMS</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr><tr>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">MMS</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr></table><br>"
                            + "<ul><li>Number of restarted campaigns</li></ul>"
                            + "<p>Measured by the number of cloned campaigns.</p>"
                            + "<table cellspacing='15' cellpadding='10' style='text-align: center;border: thin solid #2BB673;border-style: solid;border-collapse: collapse;'>"
                            + "<tr>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">OPERATOR</td>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">MESSAGE TYPE</td>"
                            + "<td style=\"font-weight:bold;width:150px;background-color: #2BB673;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\"># CAMPAIGNS</td>"
                            + "</tr><tr>"
                            + "<td rowspan='2' style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">VODAFONE DE</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">SMS</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr><tr>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">MMS</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr><tr>"
                            + "<td rowspan='2' style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">DEUTSCHE TELEKOM</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">SMS</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr><tr>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">MMS</td>"
                            + "<td style=\"width:150px;text-align:center;vertical-align: middle;border: 1px solid #D4D4D4;\">%s</td>"
                            + "</tr></table>"
                            + "<br><p><br>Best regards,</p><p>Campaigns Support Team</p><br>";

            String htmlMessage = String.format(template, vfTotal, vfSent, vfFailed,
                                                         dtTotal, dtSent, dtFailed,
                                                         totalMessages, totalSent, sentPercentage,
                                                         totalFailed, failedPercentage,
                                                         vfSMSCampaignsNum, vfMMSCampaignsNum,
                                                         dtSMSCampaignsNum, dtMMSCampaignsNum,
                                                         vfSMSClonedCampaignsNum, vfMMSClonedCampaignsNum,
                                                         dtSMSClonedCampaignsNum, dtMMSClonedCampaignsNum);
            helper.setText(htmlMessage, true);
            helper.setTo(recipientList);
            helper.setSubject(subject);
            helper.setFrom(environment.getProperty("emailFrom"));
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
