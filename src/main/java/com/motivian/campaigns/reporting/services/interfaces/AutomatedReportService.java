package com.motivian.campaigns.reporting.services.interfaces;

public interface AutomatedReportService {

    void sendWeeklyReport();

}
